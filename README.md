Files to generate **Docker** image with pyodbc (+ drivers for ms sql server and mysql) based on tiangolo/uwsgi-nginx-flask:python3.7 (= **debian stretch** + **uWSGI** + **Nginx** + **Flask** + **Python 3.7**).

You can get generated docker image at [**yangwooko/stretch-uwsgi-nginx-flask-pyodbc @ dockerhub**](https://hub.docker.com/repository/docker/yangwooko/stretch-uwsgi-nginx-flask-pyodbc).

For detail information, you'd better refer to base docker documentation at [**tiangolo/uwsgi-nginx-flask page @ github**](https://github.com/tiangolo/uwsgi-nginx-flask-docker).