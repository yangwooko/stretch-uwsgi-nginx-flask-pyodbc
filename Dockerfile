FROM tiangolo/uwsgi-nginx-flask:python3.7
RUN pip install --upgrade pip
LABEL maintainer="Yangwoo Ko <yangwooko@gmail.com>"

COPY ./app /app

# apt-get and system utilities
RUN apt-get update && apt-get install -y \
    curl apt-utils apt-transport-https debconf-utils gcc build-essential\
    && rm -rf /var/lib/apt/lists/*
# RUN apt-get update && apt-get install -y \
#     curl apt-utils apt-transport-https debconf-utils gcc build-essential g++-5\
#     && rm -rf /var/lib/apt/lists/*

# adding custom MS repository
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

# install libssl - required for sqlcmd to work on Ubuntu 18.04
# RUN apt-get update && apt-get install -y libssl1.0.0 libssl-dev

# install SQL Server drivers
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql17 unixodbc-dev

# install SQL Server tools
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"

RUN pip install pyodbc
RUN pip install pymysql